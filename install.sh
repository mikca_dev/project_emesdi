#!/bin/bash

## to get latest version of asible from epel
yum install epel-release -y
yum install ansible -y 


ansible-playbook -i hosts site.yml --connection=local  -vv
