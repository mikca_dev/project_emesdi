**Description **

Ansible scripts  

* Install Nginx, Supervisord, django and deploy app based on Django.
* app is deployed to /var/app/emesdi/ and managed by supervisord
* nginx provides https proxy for emesdi app
* use the latest version of ansible from EPEL
* tested on Centos 7.x distribution 



**Install notes **

```
yum install git -y

git clone https://mikca_dev@bitbucket.org/mikca_dev/project_emesdi.git

cd project_emesdi

change  server_hostname (or other requaired parameters in  project_emesdi/group_vars/all 

run 
./install.sh

```





---
